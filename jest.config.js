module.exports = {
  collectCoverage: true,
  forceExit: true,
  roots: [
    "<rootDir>/src"
  ],
  testMatch: [
    "**/?(*.)+(spec|test).ts"
  ],
  setupFilesAfterEnv: [
    "./src/__tests__/setupTests.ts"
  ],
  transform: {
    "^.+\\.ts": "ts-jest"
  },
  collectCoverageFrom: [
    "./src/**/*.ts",
    "!./src/__tests__/**/*.ts",
    "!./src/?(*.)+(spec|test).ts"
  ]
};
