import { IUser } from './types';

declare module 'express-serve-static-core' {
    // tslint:disable-next-line
    interface Request {
        user?: IUser;
    }
}
