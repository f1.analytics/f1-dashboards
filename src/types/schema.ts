import Dashboard from '../database/entity/dashboard';

export interface ISaveDashboardInput {
    dashboardTypeId: number;
    id?: number;
    userId?: number;
    name: string;
    icon?: string;
    config?: IDashboardConfig;
}

export interface IPutDashboardInput {
    dashboardTypeId: number;
    id: number;
    userId?: number;
    name: string;
    icon?: string;
    config?: IDashboardConfig;
}

export interface ISaveDataCardInput {
    dataCardTypeId: number;
    dashboardId: number;
}

export interface IDashboardConfig {
    defaultSubjectSelection?: {
        subjectSelectionId: number;
        filterValue?: string | number | null;
    };
}

export interface ISaveSubjectSelectionInput {
    dashboardId: number;
    config: ISubjectSelectionConfig;
}

export interface IPutSubjectSelectionInput {
    id: number;
    dashboardId?: number;
    config?: ISubjectSelectionConfig;
}

export interface ISubjectSelectionConfig extends ISelectionConfig {
    filterSelection?: ISelectionConfig;
}

export interface ISelectionConfig {
    name: string;
    dataQuery: IDataQuery;
    variables: IVariable[];
}

export interface IDataQuery {
    id: number;
    variables: IVariable[];
}

export interface IVariable {
    exportAs: string;
    exportFrom?: string;
    exportKey?: string;
    exportValue?: string | number;
    text?: string;
}

export interface IDashboardType {
    id: number;
    name: string;
    icon: string;
}

export enum DisplayTypes {
    Chart = 'chart',
    InfoBox = 'infoBox',
}

export enum DisplaySubTypes {
    LineChart = 'lineChart',
}

export interface IDataCardType {
    id: number;
    name: string;
    dataQuery: IDataQuery[];
    dashboardType: IDashboardType;
    variables: IVariable[];
    config: {
        displayType: DisplayTypes;
        displaySubType?: DisplaySubTypes;
    };
}

export interface IRichDashboard extends Dashboard {
    dashboardType: IDashboardType;
}
