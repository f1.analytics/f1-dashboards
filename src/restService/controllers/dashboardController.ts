import express, { Handler, NextFunction, Request, Response, Router } from 'express';

import { DashboardNotFoundError } from '../../errors';
import { IDashboardManager } from '../../services/dashboardManager';
import { IRequestValidator } from '../../tools/requestValidator/requestValidator.types';
import { IPutDashboardInput, IUser } from '../../types';
import { IRequestHandlerManager } from '../requestHandlersManager';
import Controller from './controller';

class DashboardsController extends Controller {
    private readonly dashboardManager: IDashboardManager;

    public constructor(
        requestHandlerManager: IRequestHandlerManager,
        requestValidator: IRequestValidator,
        dashboardManager: IDashboardManager,
    ) {
        super(requestHandlerManager, requestValidator);
        this.dashboardManager = dashboardManager;
        this.route = '/dashboards';
    }

    public resolveRouter(): Router {
        const router = express.Router();

        router.get('/:id', this.getDashboardHandler());

        router.use(this.requestHandlerManager.getAuthorizationHandler(false));

        router.get('/', this.getDashboardsHandler());

        router.use(this.requestHandlerManager.getAuthorizationHandler());

        router.post('/', this.postDashboardHandler());
        router.put('/', this.putDashboardHandler());
        router.delete('/', this.deleteDashboardsHandler());
        router.delete('/:id', this.deleteDashboardHandler());

        return router;
    }

    private getDashboardsHandler(): Handler {
        return async (req: Request, res: Response, next: NextFunction) => {
            try {
                const userId = (req.user && !req.user.isSuperuser) ? req.user.id : null;
                const dashboards = await this.dashboardManager.getDashboards(userId);
                res.send(dashboards.map((dashboard) => ({
                    dashboardTypeId: dashboard.dashboardTypeId,
                    dataCardCount: dashboard.dataCards.length,
                    id: dashboard.id,
                    name: dashboard.name,
                    userId: dashboard.userId,
                })));
            } catch (error) {
                next(error);
            }
        };
    }

    private getDashboardHandler(): Handler {
        return async (req: Request, res: Response, next: NextFunction) => {
            try {
                const dashboardId = Number(req.params.id);
                const dashboard = await this.dashboardManager.getDashboard(dashboardId);
                res.send({
                    ...dashboard,
                    dashboardTypeId: undefined,
                });
            } catch (error) {
                next(error);
            }
        };
    }

    private postDashboardHandler(): Handler {
        return async (req: Request, res: Response, next: NextFunction) => {
            try {
                const dashboard = req.body;

                this.requestValidator.validateRequestData(dashboard, 'ISaveDashboardInput');
                if (dashboard.userId) {
                    this.throwIfUnauthorizedAccess(Number(dashboard.userId), req.user);
                }
                const id = await this.dashboardManager.createDashboard(dashboard);
                res.send({ id });
            } catch (error) {
                next(error);
            }
        };
    }

    private putDashboardHandler(): Handler {
        return async (req: Request, res: Response, next: NextFunction) => {
            try {
                const dashboard = req.body;

                this.requestValidator.validateRequestData(dashboard, 'IPutDashboardInput');
                if (dashboard.userId) {
                    this.throwIfUnauthorizedAccess(Number(dashboard.userId), req.user);
                }
                await this.replaceOrCreateDashboard(dashboard, req.user);
                res.send();
            } catch (error) {
                next(error);
            }
        };
    }

    private deleteDashboardsHandler(): Handler {
        return async (req: Request, res: Response, next: NextFunction) => {
            try {
                const userId = Number(req.query.userId);
                await this.dashboardManager.deleteDashboards(userId);
                res.send();
            } catch (error) {
                next(error);
            }
        };
    }

    private deleteDashboardHandler(): Handler {
        return async (req: Request, res: Response, next: NextFunction) => {
            try {
                const dashboardId = Number(req.params.id);
                await this.dashboardManager.checkOwnership(dashboardId, req.user);
                await this.dashboardManager.deleteDashboard(dashboardId);
                res.send();
            } catch (error) {
                next(error);
            }
        };
    }

    private async replaceOrCreateDashboard(dashboard: IPutDashboardInput, requestUser: IUser): Promise<void> {
        try {
            await this.dashboardManager.checkOwnership(dashboard.id, requestUser);
            await this.dashboardManager.updateDashboard(dashboard);
        } catch (error) {
            if (error instanceof DashboardNotFoundError) {
                await this.dashboardManager.createDashboard(dashboard);
            } else {
                throw error;
            }
        }
    }
}

export default DashboardsController;
