import { Router } from 'express';

import { UnauthorizedError } from '../../errors/requestErrors';
import { IRequestValidator } from '../../tools/requestValidator/requestValidator.types';
import { IUser } from '../../types';
import { IRequestHandlerManager } from '../requestHandlersManager';
import { IController } from './controller.types';

abstract class Controller implements IController {
    protected readonly requestHandlerManager: IRequestHandlerManager;
    protected readonly requestValidator: IRequestValidator;
    protected route: string;

    public constructor(requestHandlerManager: IRequestHandlerManager, requestValidator: IRequestValidator) {
        this.requestHandlerManager = requestHandlerManager;
        this.requestValidator = requestValidator;
    }

    public abstract resolveRouter(): Router;

    public getRoute(): string {
        return this.route;
    }

    protected throwIfUnauthorizedAccess(dashboardUserId: number, requestUser: IUser): void {
        if (dashboardUserId !== Number(requestUser.id) && !requestUser.isSuperuser) {
            throw new UnauthorizedError(`User ${requestUser.id} cannot access dashboard, that belongs to ${dashboardUserId}`);
        }
    }
}

export default Controller;
