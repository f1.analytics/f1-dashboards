import express, { Handler, NextFunction, Request, Response, Router } from 'express';

import { IDataCardManager } from '../../services/dataCardManager';
import { IRequestValidator } from '../../tools/requestValidator/requestValidator.types';
import { IRequestHandlerManager } from '../requestHandlersManager';
import Controller from './controller';

class DataCardController extends Controller {
    private readonly dataCardManager: IDataCardManager;

    public constructor(
        requestHandlerManager: IRequestHandlerManager,
        requestValidator: IRequestValidator,
        dataCardManager: IDataCardManager,
    ) {
        super(requestHandlerManager, requestValidator);
        this.dataCardManager = dataCardManager;
        this.route = '/dataCards';
    }

    public resolveRouter(): Router {
        const router = express.Router();

        router.get('/:id', this.getDataCardHandler());
        router.get('/', this.getDataCardsHandler());
        router.post('/', this.postDataCardHandler());
        router.delete('/:id', this.deleteDataCardHandler());
        router.get('/data/:dataCardTypeId', this.getResolvedDataHandler());

        return router;
    }

    private getDataCardHandler(): Handler {
        return async (req: Request, res: Response, next: NextFunction) => {
            try {
                const dataCard = await this.dataCardManager.getDataCard(Number(req.params.id));
                res.send(dataCard);
            } catch (error) {
                next(error);
            }
        };
    }

    private getDataCardsHandler(): Handler {
        return async (req: Request, res: Response, next: NextFunction) => {
            try {
                const dashboardId = Number(req.query.dashboardId);
                const dataCards = await this.dataCardManager.getDataCards(dashboardId);
                res.send(dataCards);
            } catch (error) {
                next(error);
            }
        };
    }

    private postDataCardHandler(): Handler {
        return async (req: Request, res: Response, next: NextFunction) => {
            try {
                const dataCard = req.body;
                this.requestValidator.validateRequestData(dataCard, 'ISaveDataCardInput');
                const id = await this.dataCardManager.saveDataCard(dataCard);
                res.send({ id });
            } catch (error) {
                next(error);
            }
        };
    }

    private deleteDataCardHandler(): Handler {
        return async (req: Request, res: Response, next: NextFunction) => {
            try {
                const id = await this.dataCardManager.deleteDataCard(Number(req.params.id));
                res.send({ id });
            } catch (error) {
                next(error);
            }
        };
    }

    private getResolvedDataHandler(): Handler {
        return async (req: Request, res: Response, next: NextFunction) => {
            try {
                const data = await this.dataCardManager.getResolvedData(
                    Number(req.params.dataCardTypeId),
                    Number(req.query.subject),
                );
                res.send(data);
            } catch (error) {
                next(error);
            }
        };
    }
}

export default DataCardController;
