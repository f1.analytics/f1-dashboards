import express, { Handler, NextFunction, Request, Response, Router } from 'express';

import { ISubjectSelectionService } from '../../services/subjectSelectionService';
import { IRequestValidator } from '../../tools/requestValidator/requestValidator.types';
import { IRequestHandlerManager } from '../requestHandlersManager';
import Controller from './controller';

class SubjectSelectionController extends Controller {
    private readonly subjectSelectionService: ISubjectSelectionService;

    public constructor(
        requestHandlersManager: IRequestHandlerManager,
        requestValidator: IRequestValidator,
        subjectSelectionService: ISubjectSelectionService,
    ) {
        super(requestHandlersManager, requestValidator);
        this.subjectSelectionService = subjectSelectionService;
        this.route = '/subjectSelections';
    }

    public resolveRouter(): Router {
        const router = express.Router();

        router.get('/:id/filterOptions', this.getFilterOptionsHandler());
        router.get('/:id/subjects', this.getSubjectsHandler());

        return router;
    }

    private getFilterOptionsHandler(): Handler {
        return async (req: Request, res: Response, next: NextFunction) => {
            try {
                const subjectSelectionId = Number(req.params.id);
                const filters = await this.subjectSelectionService.getFilters(subjectSelectionId);
                res.send(filters);
            } catch (error) {
                next(error);
            }
        };
    }

    private getSubjectsHandler(): Handler {
        return async (req: Request, res: Response, next: NextFunction) => {
            try {
                const subjectSelectionId = Number(req.params.id);
                const subjects = await this.subjectSelectionService.getSubjects(
                    subjectSelectionId,
                    req.query.filterValue,
                );
                res.send(subjects);
            } catch (error) {
                next(error);
            }
        };
    }
}

export default SubjectSelectionController;
