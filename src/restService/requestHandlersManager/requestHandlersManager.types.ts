import { ErrorRequestHandler, RequestHandler } from 'express';

export interface IRequestHandlerManager {
    getBodyParseHandler(): RequestHandler;
    getNotFoundHandler(): RequestHandler;
    getAuthorizationHandler(isRequired?: boolean): RequestHandler;
    getErrorHandler(): ErrorRequestHandler;
}
