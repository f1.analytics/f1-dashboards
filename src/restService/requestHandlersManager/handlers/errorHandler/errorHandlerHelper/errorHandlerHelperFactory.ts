import { RequestError } from '../../../../../errors';
import { ILogger } from '../../../../../tools/logger';
import { IErrorHandlerHelper, IErrorHandlerHelperFactory } from './errorHandlerHelper.types';
import RequestErrorHandlerHelper from './errorHandlerHelpers/requestErrorHandlerHelper';
import ServerErrorHandlerHelper from './errorHandlerHelpers/serverErrorHandlerHelper';

export class ErrorHandlerHelperFactory implements IErrorHandlerHelperFactory {
    private readonly logger: ILogger;

    public constructor(logger: ILogger) {
        this.logger = logger;
    }

    public create(error: Error): IErrorHandlerHelper {
        if (error instanceof RequestError) {
            return new RequestErrorHandlerHelper(this.logger);
        } else {
            return new ServerErrorHandlerHelper(this.logger);
        }
    }
}
