import { Request, Response } from 'express';

import { RequestError } from '../../../../../../errors';
import ErrorHandlerHelper from '../errorHandlerHelper';

export default class RequestErrorHandlerHelper extends ErrorHandlerHelper {
    public handleError(error: RequestError, req: Request, res: Response): void {
        this.logError(error, req);
        this.sendResponse(error, res);
    }

    private logError(error: RequestError, req: Request): void {
        this.logger.warning('Request error', { error, req });
    }

    private sendResponse(error: RequestError, res: Response): void {
        const statusCode = error.getStatusCode();
        const data = {
            error: {
                code: error.getErrorCode(),
                message: error.getMessage(),
            },
        };

        res.status(statusCode).send(data);
    }
}
