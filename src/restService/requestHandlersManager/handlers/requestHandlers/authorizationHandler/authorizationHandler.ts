import { NextFunction, Request, RequestHandler, Response } from 'express';

import { UnauthorizedError } from '../../../../../errors';
import { ITokenManager } from '../../../../../tools/tokenManager';
import { IAuthorizationHandler } from './authorizationHandler.types';

class AuthorizationHandler implements IAuthorizationHandler {
    private readonly tokenManager: ITokenManager;

    public constructor(tokenManager: ITokenManager) {
        this.tokenManager = tokenManager;
    }

    public getHandler(isRequired = true): RequestHandler {
        return (req: Request, res: Response, next: NextFunction) => {
            const authorization = req.headers.authorization;
            if (authorization && authorization.startsWith('Bearer ')) {
                const accessToken = authorization.slice(7, authorization.length);
                const { payload } = this.tokenManager.validateJwtToken(accessToken);
                req.user = payload;
            } else if (isRequired) {
                throw new UnauthorizedError('Missing access token in authorization header');
            }

            next();
        };
    }
}

export default AuthorizationHandler;
