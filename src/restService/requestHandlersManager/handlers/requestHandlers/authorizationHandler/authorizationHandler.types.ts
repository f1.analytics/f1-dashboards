import { RequestHandler } from 'express';

export interface IAuthorizationHandler {
    getHandler(isRequired?: boolean): RequestHandler;
}
