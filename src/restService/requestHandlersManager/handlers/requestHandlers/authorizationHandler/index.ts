import RequiredAuthorizationHandler from './authorizationHandler';

export * from './authorizationHandler.types';
export default RequiredAuthorizationHandler;
