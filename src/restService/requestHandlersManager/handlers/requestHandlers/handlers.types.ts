import { RequestHandler } from 'express';

export interface IRequestHandler {
    getHandler(): RequestHandler;
}
