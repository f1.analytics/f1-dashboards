import CompositionRoot from '../../compositionRoot';
import { IRestService } from '../../restService';
import { IDashboardManager } from '../../services/dashboardManager';
import {
    mockDashboard1,
    mockDashboard2,
    mockDashboard3,
    mockDashboard4,
    mockDashboard5,
} from './integration.test.data';

export const initCompositionRoot = async (): Promise<CompositionRoot> => {
    const compositionRoot = new CompositionRoot();
    await compositionRoot.createServer({
        api: {
            data: 'http://localhost:8083',
            settings: 'http://localhost:8084',
        },
        logger: {
            level: 'info',
        },
        persistence: {
            database: ':memory:',
            dropSchema: true,
            logging: false,
            type: 'sqlite',
        },
        server: {
            host: 'test',
            port: 1234,
        },

    });
    return compositionRoot;
};

export const initRestService = (compositionRoot: CompositionRoot): IRestService => {
    const restService = compositionRoot.getRestService();
    restService.init();
    return restService;
};

export const prepareMockDashboards = async (dashboardManager: IDashboardManager) => {
    await Promise.all([
        mockDashboard1,
        mockDashboard2,
        mockDashboard3,
        mockDashboard4,
        mockDashboard5,
    ].map((mockDashboard) => dashboardManager.createDashboard(mockDashboard)));
};
