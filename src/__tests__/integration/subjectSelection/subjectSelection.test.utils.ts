import DataClient from '../../../clients/dataClient';
import SettingsClient from '../../../clients/settingsClient';

export const mockSubjectSelectionResponse = (response: any) => {
    jest.spyOn(SettingsClient.prototype, 'getSubjectSelection')
        .mockImplementation(() => Promise.resolve(response));
};
export const mockDashboardTypeResponse = (response: any) => {
    jest.spyOn(SettingsClient.prototype, 'getDashboardType')
        .mockImplementation(() => Promise.resolve(response));
};
export const mockDataResponse = (response: any) => {
    jest.spyOn(DataClient.prototype, 'getData').mockImplementation(() => Promise.resolve(response));
};
