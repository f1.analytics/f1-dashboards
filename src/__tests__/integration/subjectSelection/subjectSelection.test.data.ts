import { ExportFromTypes } from '../../../types';

export const getDriversSubjectSelection = {
    config: {
        selectionTypePlaceholder: 'All',
        subjectSelect: {
            dataQuery: {
                id: 1,
                variables: [] as any,
            },
            placeholder: 'Select Driver',
            variables: [
                {
                    exportAs: 'key',
                    exportFrom: ExportFromTypes.DataQuery,
                    exportKey: 'id',
                },
                {
                    exportAs: 'text',
                    exportFrom: ExportFromTypes.DataQuery,
                    exportKey: 'surname',
                },
                {
                    exportAs: 'value',
                    exportFrom: ExportFromTypes.DataQuery,
                    exportKey: 'id',
                },
            ],
        },
    },
    id: 1,
    name: 'All Drivers',
};
export const getDriversByNationalitySubjectSelection = {
    config: {
        filterSelect: {
            dataQuery: {
                id: 2,
                variables: [] as any,
            },
            placeholder: 'Select Nationality',
            variables: [
                {
                    exportAs: 'key',
                    exportFrom: ExportFromTypes.DataQuery,
                    exportKey: 'nationality',
                },
                {
                    exportAs: 'text',
                    exportFrom: ExportFromTypes.DataQuery,
                    exportKey: 'nationality',
                },
                {
                    exportAs: 'value',
                    exportFrom: ExportFromTypes.DataQuery,
                    exportKey: 'nationality',
                },
            ],
        },
        selectionTypePlaceholder: 'By Nationality',
        subjectSelect: {
            dataQuery: {
                id: 3,
                variables: [{
                    exportAs: 'nationality',
                    exportFrom: ExportFromTypes.FilterSelection,
                    exportKey: 'value',
                }],
            },
            placeholder: 'Select Driver',
            variables: [
                {
                    exportAs: 'key',
                    exportFrom: ExportFromTypes.DataQuery,
                    exportKey: 'id',
                },
                {
                    exportAs: 'text',
                    exportFrom: ExportFromTypes.DataQuery,
                    exportKey: 'surname',
                },
                {
                    exportAs: 'value',
                    exportFrom: ExportFromTypes.DataQuery,
                    exportKey: 'id',
                },
            ],
        },
    },
    id: 2,
    name: 'Drivers by Nationality',
};

export const driversResponse = [{
    forename: 'Fernando',
    id: 1,
    surname: 'Alonso',
}, {
    forename: 'Mark',
    id: 2,
    surname: 'Webber',
}];
export const driversByNationalityResponse = [{
    forename: 'Fernando',
    id: 1,
    surname: 'Alonso',
}];
export const nationalitiesResponse = [{
    nationality: 'French',
}, {
    nationality: 'Spanish',
}, {
    nationality: 'German',
}];

export const expectedDriversResponse = [{
    key: 1,
    text: 'Alonso',
    value: 1,
}, {
    key: 2,
    text: 'Webber',
    value: 2,
}];
export const expectedDriversByNationalityResponse = [{
    key: 1,
    text: 'Alonso',
    value: 1,
}];
export const expectedNationalitiesResponse = [{
    key: 'French',
    text: 'French',
    value: 'French',
}, {
    key: 'Spanish',
    text: 'Spanish',
    value: 'Spanish',
}, {
    key: 'German',
    text: 'German',
    value: 'German',
}];
