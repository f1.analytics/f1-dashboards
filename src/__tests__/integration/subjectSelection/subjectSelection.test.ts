import { Express } from 'express';
import request from 'supertest';

import DataClient from '../../../clients/dataClient';
import { initCompositionRoot, initRestService } from '../integration.test.utils';
import {
    driversByNationalityResponse,
    driversResponse,
    expectedDriversByNationalityResponse,
    expectedDriversResponse,
    expectedNationalitiesResponse,
    getDriversByNationalitySubjectSelection,
    getDriversSubjectSelection,
    nationalitiesResponse,
} from './subjectSelection.test.data';
import { mockDataResponse, mockSubjectSelectionResponse } from './subjectSelection.test.utils';

describe('Subject Selection API tests', () => {
    let app: Express;

    beforeAll(async () => {
        const compositionRoot = await initCompositionRoot();
        const restService = await initRestService(compositionRoot);
        app = restService.getApp();
    });

    describe('GET /subjectSelections/:id/subjects tests', () => {
        it('Should resolve subjects', () => {
            mockSubjectSelectionResponse(getDriversSubjectSelection);
            mockDataResponse(driversResponse);
            return request(app)
                .get('/subjectSelections/1/subjects')
                .expect(200)
                .expect(expectedDriversResponse)
                .expect(() => {
                    expect(DataClient.prototype.getData).toHaveBeenCalledWith(
                        getDriversSubjectSelection.config.subjectSelect.dataQuery.id, {},
                    );
                });
        });
        it('Should resolve subjects with query params', () => {
            mockSubjectSelectionResponse(getDriversByNationalitySubjectSelection);
            mockDataResponse(driversByNationalityResponse);
            return request(app)
                .get('/subjectSelections/1/subjects?filterValue=Spanish')
                .expect(200)
                .expect(expectedDriversByNationalityResponse)
                .expect(() => {
                    expect(DataClient.prototype.getData).toHaveBeenCalledWith(
                        getDriversByNationalitySubjectSelection.config.subjectSelect.dataQuery.id,
                        { nationality: 'Spanish' },
                    );
                });
        });
    });
    describe('GET /subjectSelections/:id/filterOptions tests', () => {
        it('Should resolve filters', () => {
            mockSubjectSelectionResponse(getDriversByNationalitySubjectSelection);
            mockDataResponse(nationalitiesResponse);
            return request(app)
                .get('/subjectSelections/1/filterOptions')
                .expect(200)
                .expect(expectedNationalitiesResponse)
                .expect(() => {
                    expect(DataClient.prototype.getData).toHaveBeenCalledWith(
                        getDriversByNationalitySubjectSelection.config.filterSelect.dataQuery.id,
                    );
                });
        });
    });
});
