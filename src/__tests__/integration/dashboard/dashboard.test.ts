import { Express } from 'express';
import jwt from 'jsonwebtoken';
import request from 'supertest';

import { DashboardNotFoundError } from '../../../errors/requestErrors';
import { IDashboardManager } from '../../../services/dashboardManager';
import {
    mockDashboard1,
    mockDashboard2,
    mockDashboard3,
    mockDashboard4,
    mockDashboard5, mockDashboardType,
} from '../integration.test.data';
import { initCompositionRoot, initRestService, prepareMockDashboards } from '../integration.test.utils';
import { mockDashboardTypeResponse } from '../subjectSelection/subjectSelection.test.utils';
import {
    expectedInvalidDeleteDashboardResponse,
    expectedInvalidSaveDashboardResponse,
    expectedInvalidUpdateDashboardResponse,
    expectedNotFoundDashboardResponse,
    expectedUnauthorizedSaveDashboardResponse,
    expectedWithoutAuthHeaderSaveDashboardResponse,
    mockInvalidSaveDashboardInput,
    mockValidInsertDashboardInput,
    mockValidSaveDashboardInput,
    mockValidUpdateDashboardInput,
} from './dashboard.test.data';

describe('Dashboard API tests', () => {
    let app: Express;
    let dashboardManager: IDashboardManager;

    beforeAll(async () => {
        const compositionRoot = await initCompositionRoot();
        const restService = await initRestService(compositionRoot);
        app = restService.getApp();
        dashboardManager = compositionRoot.getDashboardManager();

        await prepareMockDashboards(dashboardManager);

        jest.spyOn(jwt, 'verify').mockImplementation((id) => ({
            payload: {
                id: Number(id),
                isSuperuser: id === '0',
            },
        }));
    });

    describe('GET / tests', () => {
        it('Should get user dashboards', () => {
            return request(app)
                .get('/dashboards')
                .set('Authorization', `Bearer ${mockDashboard1.userId}`)
                .expect(200)
                .expect((res) => {
                    expect(res.body[0]).toMatchObject(mockDashboard1);
                    expect(res.body[1]).toMatchObject(mockDashboard2);
                });
        });
        it('Should get default dashboards', () => {
            return request(app)
                .get('/dashboards/')
                .set('Authorization', 'Bearer 0')
                .expect(200)
                .expect((res) => {
                    expect(res.body[0]).toMatchObject(mockDashboard3);
                    expect(res.body[1]).toMatchObject(mockDashboard4);
                });
        });
    });

    describe('GET /:id tests', () => {
        it('Should get dashboard', () => {
            mockDashboardTypeResponse(mockDashboardType);
            return request(app)
                .get(`/dashboards/${mockDashboard1.id}`)
                .expect(200)
                .expect((res) => {
                    const expectedResult = { ...mockDashboard1 };
                    delete expectedResult.dashboardTypeId;
                    expect(res.body).toMatchObject(expectedResult);
                });
        });
        it('Should fail to get dashboard if it does not exist', () => {
            return request(app)
                .get('/dashboards/123124324')
                .expect(404)
                .expect(expectedNotFoundDashboardResponse);
        });
    });

    describe('POST / tests', () => {
        it('Should create a new dashboard',  () => {
            return request(app)
                .post('/dashboards/')
                .set('Authorization', 'Bearer 1')
                .send(mockValidSaveDashboardInput)
                .expect(200)
                .then(async (res) => {
                    const dashboard = await dashboardManager.getDashboard(res.body.id);
                    expect(dashboard).toMatchObject(mockValidSaveDashboardInput);
                });
        });
        it('Should fail to create a new dashboard with invalid request',  () => {
            return request(app)
                .post('/dashboards/')
                .set('Authorization', 'Bearer 1')
                .send(mockInvalidSaveDashboardInput)
                .expect(400)
                .expect(expectedInvalidSaveDashboardResponse);
        });
        it('Should fail to create a new dashboard if authorized user does not match',  () => {
            return request(app)
                .post('/dashboards/')
                .set('Authorization', 'Bearer 1')
                .send(mockDashboard5)
                .expect(401)
                .expect(expectedUnauthorizedSaveDashboardResponse);
        });
        it('Should fail to create a new dashboard if authorized is missing',  () => {
            return request(app)
                .post('/dashboards/')
                .send(mockDashboard5)
                .expect(401)
                .expect(expectedWithoutAuthHeaderSaveDashboardResponse);
        });
    });

    describe('PUT / tests', () => {
        it('Should save a new dashboard',  () => {
            return request(app)
                .put('/dashboards/')
                .set('Authorization', `Bearer ${mockValidInsertDashboardInput.userId}`)
                .send(mockValidInsertDashboardInput)
                .expect(200)
                .then(async () => {
                    const dashboards = await dashboardManager.getDashboards(mockValidInsertDashboardInput.userId);
                    expect(dashboards[0]).toMatchObject(mockValidInsertDashboardInput);
                });
        });
        it('Should fail to save a new dashboard with invalid request',  () => {
            return request(app)
                .put('/dashboards/')
                .set('Authorization', 'Bearer 1')
                .send(mockInvalidSaveDashboardInput)
                .expect(400)
                .expect(expectedInvalidSaveDashboardResponse);
        });
        it('Should update existing dashboard',  () => {
            return request(app)
                .put('/dashboards/')
                .set('Authorization', `Bearer ${mockValidUpdateDashboardInput.userId}`)
                .send(mockValidUpdateDashboardInput)
                .expect(200)
                .then(async () => {
                    const dashboard = await dashboardManager.getDashboard(mockValidUpdateDashboardInput.id);
                    expect(dashboard).toMatchObject(mockValidUpdateDashboardInput);
                });
        });
        it('Should fail to update existing dashboard',  () => {
            return request(app)
                .put('/dashboards/')
                .set('Authorization', 'Bearer 1')
                .send(mockInvalidSaveDashboardInput)
                .expect(400)
                .expect(expectedInvalidUpdateDashboardResponse);
        });
    });

    describe('DELETE / tests', () => {
        it('Should delete user dashboards', () => {
            return request(app)
                .delete(`/dashboards/?userId=${mockDashboard1.userId}`)
                .set('Authorization', `Bearer ${mockDashboard1.userId}`)
                .expect(200)
                .expect(async () => {
                    const dashboards = await dashboardManager.getDashboards(mockDashboard1.userId);
                    expect(dashboards).toEqual([]);
                });
        });
    });

    describe('DELETE /:id tests', () => {
        it('Should fail to delete dashboard that is not owned by user', () => {
            return request(app)
                .delete(`/dashboards/${mockDashboard5.id}`)
                .set('Authorization', 'Bearer 33333')
                .expect(401)
                .expect(expectedInvalidDeleteDashboardResponse);
        });
        it('Should fail to delete not existing dashboard', () => {
            return request(app)
                .delete('/dashboards/123124324')
                .set('Authorization', 'Bearer 33333')
                .expect(404)
                .expect(expectedNotFoundDashboardResponse);
        });
        it('Should delete dashboard', () => {
            return request(app)
                .delete(`/dashboards/${mockDashboard5.id}`)
                .set('Authorization', `Bearer ${mockDashboard5.userId}`)
                .expect(200)
                .expect(async () => {
                    expect(dashboardManager.getDashboard(mockDashboard5.id)).toThrowError(DashboardNotFoundError);
                });
        });
    });
});
