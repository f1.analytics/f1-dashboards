import { errorCodes } from '../../../errors';
import { mockDashboard1 } from '../integration.test.data';

export const mockValidSaveDashboardInput = {
    dashboardTypeId: 2,
    name: 'test1',
};
export const mockInvalidSaveDashboardInput = {
    icon: 'test',
};
export const mockValidInsertDashboardInput = {
    dashboardTypeId: 3,
    id: 50,
    name: 'insertedDevice',
    userId: 10,
};
export const mockValidUpdateDashboardInput = {
    ...mockDashboard1,
    id: 1123,
    name: 'new name',
};

export const expectedNotFoundDashboardResponse = {
    error: {
        code: errorCodes.DASHBOARD_NOT_FOUND,
        message: 'Dashboard 123124324 not found',
    },
};
export const expectedInvalidSaveDashboardResponse = {
    error: {
        code: errorCodes.BAD_REQUEST,
        message: 'should have required property \'dashboardTypeId\'',
    },
};
export const expectedUnauthorizedSaveDashboardResponse = {
    error: {
        code: 'Unauthorized',
        message: 'User 1 cannot access dashboard, that belongs to 14213',
    },
};
export const expectedWithoutAuthHeaderSaveDashboardResponse = {
    error: {
        code: 'Unauthorized',
        message: 'Missing access token in authorization header',
    },
};
export const expectedInvalidUpdateDashboardResponse = {
    error: {
        code: errorCodes.BAD_REQUEST,
        message: 'should have required property \'dashboardTypeId\'',
    },
};
export const expectedInvalidDeleteDashboardResponse = {
    error: {
        code: errorCodes.UNAUTHORIZED,
        message: 'User 33333 is not authorized to access dashboard 204',
    },
};
