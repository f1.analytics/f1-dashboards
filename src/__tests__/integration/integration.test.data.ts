import { DisplaySubTypes, DisplayTypes, IDataCardType, IVariable } from '../../types';

const userId1 = 68;

export const mockDashboard1 = {
    dashboardTypeId: 1,
    id: 200,
    name: 'name1',
    userId: userId1,
};
export const mockDashboard2 = {
    dashboardTypeId: 1,
    id: 201,
    name: 'name2',
    userId: userId1,
};
export const mockDashboard3 = {
    dashboardTypeId: 1,
    id: 202,
    name: 'name3',
};
export const mockDashboard4 = {
    dashboardTypeId: 1,
    id: 203,
    name: 'name4',
};
export const mockDashboard5 = {
    dashboardTypeId: 1,
    id: 204,
    name: 'name5',
    userId: 14213,
};

export const mockSubjectSelection = {
    config: {
        selectionTypePlaceholder: 'All',
        subjectSelect: {
            dataQuery: {
                id: 3,
                variables: [] as IVariable[],
            },
            placeholder: 'Select driver',
            variables: [
                {
                    exportAs: 'value',
                    exportFrom: 'dataQuery',
                    exportKey: 'id',
                },
                {
                    exportAs: 'text',
                    exportFrom: 'dataQuery',
                    exportKey: 'name',
                },
            ],
        },
    },
    createDate: '2020-04-26T09:42:53.255Z',
    id: 1,
    name: 'Get drivers',
};

export const mockDataCardType = {
    config: {
        displaySubType: DisplaySubTypes.LineChart,
        displayType: DisplayTypes.Chart,
        xFormat: 'year',
        xText: 'Season',
        yText: 'Wins count',
    },
    createDate: '2020-04-26T09:42:55.180Z',
    dashboardType: {
        icon: 'helmet',
        id: 1,
        name: 'Drivers',
    },
    dataQuery: [
        {
            id: 6,
            variables: [
                {
                    exportAs: 'driverId',
                    exportFrom: 'subject',
                },
                {
                    exportAs: 'queryKey',
                    exportValue: 'position',
                },
                {
                    exportAs: 'queryValues',
                    exportValue: '1',
                },
            ],
        },
    ],
    id: 1,
    name: 'Driver race wins',
    variables: [
        {
            exportAs: 'x',
            exportFrom: 'dataQuery',
            exportKey: 'year',
        },
        {
            exportAs: 'y',
            exportFrom: 'dataQuery',
            exportKey: 'resultCount',
        },
        {
            exportAs: 'yMax',
            exportFrom: 'dataQuery',
            exportKey: 'raceCount',
        },
    ],
} as IDataCardType;

export const mockDashboardType = {
    dataCardTypes: [mockDataCardType],
    icon: 'helmet',
    id: 1,
    name: 'Drivers',
    subjectSelections: [mockSubjectSelection],
};
