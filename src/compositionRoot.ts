import DataClient from './clients/dataClient';
import SettingsClient from './clients/settingsClient';
import Persistence from './database/persistence';
import RestService, { IRestService } from './restService';
import DashboardController from './restService/controllers/dashboardController';
import DataCardController from './restService/controllers/dataCardController';
import SubjectSelectionController from './restService/controllers/subjectSelectionController';
import RequestHandlersManager from './restService/requestHandlersManager';
import ErrorHandler from './restService/requestHandlersManager/handlers/errorHandler';
import { ErrorHandlerHelperFactory } from './restService/requestHandlersManager/handlers/errorHandler/errorHandlerHelper';
import RequiredAuthorizationHandler from './restService/requestHandlersManager/handlers/requestHandlers/authorizationHandler';
import BodyParseHandler from './restService/requestHandlersManager/handlers/requestHandlers/bodyParseHandler';
import NotFoundHandler from './restService/requestHandlersManager/handlers/requestHandlers/notFoundHandler';
import Server from './server';
import DashboardManager, { IDashboardManager } from './services/dashboardManager';
import DataCardManager from './services/dataCardManager';
import SubjectSelectionService from './services/subjectSelectionService/subjectSelectionService';
import Logger from './tools/logger';
import RequestValidator from './tools/requestValidator/requestValidator';
import SchemaGenerator from './tools/schemaGenerator';
import SchemaValidator from './tools/schemaValidator';
import SecretsManger from './tools/secretsManager';
import TokenManager from './tools/tokenManager';
import { IConfig } from './types';

class CompositionRoot {
    private dashboardManager: IDashboardManager;
    private restService: IRestService;
    private server: Server;

    public async createServer(config: IConfig): Promise<void> {
        const logger = new Logger(config.logger);
        const secretsManager = new SecretsManger(logger);

        const persistence = new Persistence(config.persistence, secretsManager);

        await persistence.init();

        const tokenManager = new TokenManager(secretsManager);
        const schemaGenerator = new SchemaGenerator();
        const schemaValidator = new SchemaValidator();
        const requestValidator = new RequestValidator(schemaGenerator, schemaValidator);

        const dataClient = new DataClient(config.api.data, tokenManager);
        const settingsClient = new SettingsClient(config.api.settings, tokenManager);

        const errorHandlerHelperFactory = new ErrorHandlerHelperFactory(logger);

        const bodyParseHandler = new BodyParseHandler();
        const notFoundHandler = new NotFoundHandler();
        const authorizationHandler = new RequiredAuthorizationHandler(tokenManager);
        const errorHandler = new ErrorHandler(errorHandlerHelperFactory);
        const requestHandlerManager = new RequestHandlersManager(
            bodyParseHandler,
            notFoundHandler,
            authorizationHandler,
            errorHandler,
        );

        const dataCardsManager = new DataCardManager(settingsClient, dataClient);
        this.dashboardManager = new DashboardManager(settingsClient, dataCardsManager);
        const subjectSelectionService = new SubjectSelectionService(settingsClient, dataClient);

        const dashboardController = new DashboardController(
            requestHandlerManager,
            requestValidator,
            this.dashboardManager,
        );
        const subjectSelectionController = new SubjectSelectionController(
            requestHandlerManager,
            requestValidator,
            subjectSelectionService,
        );
        const dataCardController = new DataCardController(requestHandlerManager, requestValidator, dataCardsManager);

        this.restService = new RestService(
            config.server,
            logger,
            requestHandlerManager,
            [dashboardController, subjectSelectionController, dataCardController],
        );

        this.server = new Server(this.restService);
    }

    public getServer(): Server {
        return this.server;
    }

    public getRestService(): IRestService {
        return this.restService;
    }

    public getDashboardManager(): IDashboardManager {
        return this.dashboardManager;
    }
}

export default CompositionRoot;
