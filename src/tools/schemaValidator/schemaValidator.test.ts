import Ajv from 'ajv';

import { BadRequestError } from '../../errors/requestErrors';
import SchemaValidator from './schemaValidator';
import { mockErrors, mockObject, mockSchema } from './schemaValidator.test.data';

describe('Schema validator tests', () => {
    const schemaValidator = new SchemaValidator();

    it('Should successfully validate object', () => {
        const validateMock = jest.fn(() => true);
        const compileSpy = jest.spyOn(Ajv.prototype, 'compile').mockImplementation(() => validateMock);
        schemaValidator.validate(mockSchema, mockObject);
        expect(compileSpy).toHaveBeenCalledWith(mockSchema);
        expect(validateMock).toHaveBeenCalledWith(mockObject);
    });
    it('Should throw error on unsuccessful validation', () => {
        const validateFunc = () => false;
        validateFunc.errors = mockErrors;
        const compileSpy = jest.spyOn(Ajv.prototype, 'compile').mockImplementation(() => validateFunc);

        expect(() => schemaValidator.validate(mockSchema, mockObject)).toThrowError(BadRequestError);
        expect(compileSpy).toHaveBeenCalledWith(mockSchema);
    });
});
