import Ajv from 'ajv';

import { BadRequestError } from '../../errors/requestErrors';
import { ISchemaValidator } from './schemaValidator.types';

class SchemaValidator implements ISchemaValidator {
    public validate(schema: object, data: object): void {
        const ajv = new Ajv();
        const validate = ajv.compile(schema);
        const valid = validate(data);
        if (!valid) {
            throw new BadRequestError(validate?.errors[0].message);
        }
    }
}

export default SchemaValidator;
