import RequestValidator from './requestValidator';
import {
    mockObject,
    mockSchema,
    mockSchemaGenerator,
    mockSchemaName,
    mockSchemaValidator,
} from './requestValidator.test.data';

describe('Request validator tests', () => {
    const requestValidator = new RequestValidator(mockSchemaGenerator, mockSchemaValidator);

    it('Should generate and validate schema', () => {
        requestValidator.validateRequestData(mockObject, mockSchemaName);
        expect(mockSchemaGenerator.generate).toHaveBeenCalledWith(mockSchemaName);
        expect(mockSchemaValidator.validate).toHaveBeenCalledWith(mockSchema, mockObject);
    });
});
