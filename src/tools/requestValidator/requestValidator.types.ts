export interface IRequestValidator {
    validateRequestData(data: object, typeName: string): void;
}
