import { Definition } from 'typescript-json-schema';

export const mockSchemaName = 'testSchema';
export const mockSchema = { schema: 'test' } as Definition;
export const mockObject = { name: 'testObject' };

export const mockSchemaGenerator = {
    generate: jest.fn(() => mockSchema),
};
export const mockSchemaValidator = {
    validate: jest.fn(),
};
