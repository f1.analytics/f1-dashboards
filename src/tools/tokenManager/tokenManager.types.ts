export interface ITokenManager {
    validateJwtToken(token: string): any;
    getStaticToken(): string;
}
