import fs from 'fs';

import { ILogger } from '../logger';
import { ISecretsManager } from './secretsManager.types';

class SecretsManger implements ISecretsManager {
    private readonly logger: ILogger;

    public constructor(logger: ILogger) {
        this.logger = logger;
    }

    public get(secret: string) {
        try {
            return fs.readFileSync(`/run/secrets/${secret}`, 'utf8').trim();
        } catch (error) {
            this.logger.info(`Secret ${secret} not found, using environment variable`);
            return process.env[secret];
        }
    }
}

export default SecretsManger;
