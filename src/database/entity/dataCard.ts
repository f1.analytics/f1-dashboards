import { Column, CreateDateColumn, Entity, ManyToOne, PrimaryGeneratedColumn, RelationId } from 'typeorm';

import Dashboard from './dashboard';

@Entity()
class DataCard {
    @PrimaryGeneratedColumn()
    public id: number;

    @Column()
    public dataCardTypeId: number;

    @CreateDateColumn()
    public createDate: string;

    @ManyToOne(
        () => Dashboard,
        (dashboard) => dashboard.dataCards,
        { onDelete: 'CASCADE' },
    )
    public dashboard: Dashboard;
}

export default DataCard;
