import { Column, CreateDateColumn, Entity, OneToMany, PrimaryGeneratedColumn } from 'typeorm';

import { IDashboardConfig } from '../../types';
import DataCard from './dataCard';

@Entity()
class Dashboard {
    @PrimaryGeneratedColumn()
    public id: number;

    @Column()
    public dashboardTypeId: number;

    @Column({
        nullable: true,
    })
    public userId: number;

    @Column('text')
    public name: string;

    @Column({
        type: 'simple-json',
    })
    public config: IDashboardConfig;

    @CreateDateColumn()
    public createDate: string;

    @OneToMany(() => DataCard, (dataCard) => dataCard.dashboard)
    public dataCards: DataCard[];
}

export default Dashboard;
