import { EntityRepository, Repository } from 'typeorm';

import { IPutDashboardInput, ISaveDashboardInput } from '../../types';
import Dashboard from '../entity/dashboard';
import { relations } from './repository.types';

@EntityRepository(Dashboard)
class DashboardRepository extends Repository<Dashboard> {
    public async getDashboards(userId?: number): Promise<Dashboard[]> {
        return await this.find({
            relations: [relations.dataCards],
            where: {
                userId: userId || null,
            },
        });
    }

    public async getDashboard(id: number): Promise<Dashboard> {
        return await this.findOneOrFail({
            relations: [relations.dataCards],
            where: {
                id,
            },
        });
    }

    public async saveDashboard(dashboardInput: ISaveDashboardInput): Promise<Dashboard> {
        const dashboard = this.create({
            ...dashboardInput,
            config: dashboardInput.config || {},
        });
        return this.save(dashboard);
    }

    public async updateDashboard(dashboardInput: IPutDashboardInput): Promise<void> {
        await this.update(dashboardInput.id, dashboardInput);
    }

    public async deleteDashboards(userId: number): Promise<void> {
        await this.delete({ userId });
    }

    public async deleteDashboard(id: number): Promise<any> {
        await this.delete(id);
    }
}

export default DashboardRepository;
