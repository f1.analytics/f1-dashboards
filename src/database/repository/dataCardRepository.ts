import { EntityRepository, getCustomRepository, Repository } from 'typeorm';

import { ISaveDataCardInput } from '../../types';
import DataCard from '../entity/dataCard';
import DashboardRepository from './dashboardRepository';

@EntityRepository(DataCard)
class DataCardRepository extends Repository<DataCard> {
    public async getDataCard(id: number): Promise<DataCard> {
        return await this.findOne({
            where: {
                id,
            },
        });
    }

    public async getDataCards(dashboardId: number): Promise<DataCard[]> {
        return await this.find({
            where: {
                dashboardId,
            },
        });
    }

    public async saveDataCard(dataCardInput: ISaveDataCardInput): Promise<DataCard> {
        const dashboard = await getCustomRepository(DashboardRepository).getDashboard(dataCardInput.dashboardId);
        const dataCard = this.create({ ...dataCardInput, dashboard });
        return this.save(dataCard);
    }

    public async deleteDataCard(id: number): Promise<void> {
        await this.delete(id);
    }
}

export default DataCardRepository;
