import { IError } from '../errors.types';

export interface IRequestError extends IError {
    getStatusCode(): number;
    getErrorCode(): string;
}
