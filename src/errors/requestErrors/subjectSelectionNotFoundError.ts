import { errorCodes, statusCodes } from '../errors.types';
import { RequestError } from './requestError';

export class SubjectSelectionNotFoundError extends RequestError {
    public constructor(...args: any) {
        super(...args);

        this.statusCode = statusCodes.NOT_FOUND;
        this.errorCode = errorCodes.SUBJECT_SELECTION_NOT_FOUND;
    }
}
