export interface IError {
    getMessage(): string;
}

export enum statusCodes {
    BAD_REQUEST = 400,
    UNAUTHORIZED = 401,
    NOT_FOUND = 404,
    INTERNAL_SERVER_ERROR = 500,
}

export enum errorCodes {
    BAD_REQUEST = 'BadRequest',
    DASHBOARD_NOT_FOUND = 'DashboardNotFound',
    INTERNAL_SERVER_ERROR = 'InternalServerError',
    NOT_FOUND = 'NotFound',
    SUBJECT_SELECTION_NOT_FOUND = 'SubjectSelectionNotFound',
    UNAUTHORIZED = 'Unauthorized',
}
