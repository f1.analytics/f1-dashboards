export interface ISubjectSelectionService {
    getSubjects(subjectSelectionId: number, filterValue: string): Promise<any>;
    getFilters(subjectSelectionId: number): Promise<any>;
}

export interface ISubjectSelection {
    id: number;
    name: string;
    config: ISubjectSelectionConfig;
}

export interface ISubjectSelectionConfig {
    selectionTypePlaceholder: string;
    filterSelect?: ISelectConfig;
    subjectSelect: ISelectConfig;
}

export interface ISelectConfig {
    placeholder: string;
    dataQuery: IDataQuery;
    variables: IVariable[];
}

export interface IDataQuery {
    id: number;
    variables: IVariable[];
}

export interface IVariable {
    exportAs: string;
    exportFrom?: string;
    exportKey?: string;
    exportValue?: string | number;
}
