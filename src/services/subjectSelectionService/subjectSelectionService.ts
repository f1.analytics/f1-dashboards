import { IDataClient } from '../../clients/dataClient';
import { ISettingsClient } from '../../clients/settingsClient';
import { ExportFromTypes } from '../../types';
import { ISubjectSelectionService } from './subjectSelectionService.types';

class SubjectSelectionService implements ISubjectSelectionService {
    private readonly settingsClient: ISettingsClient;
    private readonly dataClient: IDataClient;

    public constructor(settingsClient: ISettingsClient, dataClient: IDataClient) {
        this.settingsClient = settingsClient;
        this.dataClient = dataClient;
    }

    public async getSubjects(subjectSelectionId: number, filterValue: string): Promise<any> {
        const subjectSelection = await this.settingsClient.getSubjectSelection(subjectSelectionId);
        const queryParams = subjectSelection.config.subjectSelect.dataQuery.variables.reduce((res, variable) => {
            if (variable.exportFrom === ExportFromTypes.FilterSelection) {
                return {
                    ...res,
                    [variable.exportAs]: filterValue,
                };
            }
            if (variable.exportValue) {
                return {
                    ...res,
                    [variable.exportAs]: variable.exportValue,
                };
            }
        }, {});
        const rawData = await this.dataClient.getData(subjectSelection.config.subjectSelect.dataQuery.id, queryParams);
        return rawData.map((dataItem: any) => {
            return subjectSelection.config.subjectSelect.variables.reduce((res, variable) => {
                if (variable.exportFrom === ExportFromTypes.DataQuery) {
                    return {
                        ...res,
                        [variable.exportAs]: dataItem[variable.exportKey],
                    };
                }
            }, {});
        });
    }

    public async getFilters(subjectSelectionId: number): Promise<any> {
        const subjectSelection = await this.settingsClient.getSubjectSelection(subjectSelectionId);
        const rawData = await this.dataClient.getData(subjectSelection.config.filterSelect.dataQuery.id);
        return rawData.map((dataItem: any) => {
            return subjectSelection.config.filterSelect.variables.reduce((res, variable) => {
                if (variable.exportFrom === ExportFromTypes.DataQuery) {
                    return {
                        ...res,
                        [variable.exportAs]: dataItem[variable.exportKey],
                    };
                }
            }, {});
        });
    }
}

export default SubjectSelectionService;
