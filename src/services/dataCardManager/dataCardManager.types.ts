import DataCard from '../../database/entity/dataCard';
import { ISaveDataCardInput } from '../../types';

export interface IDataCardManager {
    getDataCard(id: number): Promise<DataCard>;
    getDataCards(dashboardTypeId: number): Promise<DataCard[]>;
    saveDataCard(dataCardInput: ISaveDataCardInput): Promise<number>;
    deleteDataCard(id: number): Promise<void>;
    getResolvedData(dataCardTypeId: number, subject: number): Promise<any>;
}
