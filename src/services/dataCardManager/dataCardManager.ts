import { getCustomRepository } from 'typeorm';

import { IDataClient } from '../../clients/dataClient';
import { ISettingsClient } from '../../clients/settingsClient';
import DataCard from '../../database/entity/dataCard';
import DataCardRepository from '../../database/repository/dataCardRepository';
import { DisplayTypes, ExportFromTypes, IDataCardType, ISaveDataCardInput } from '../../types';
import { IDataCardManager } from './dataCardManager.types';

class DataCardManager implements IDataCardManager {
    private readonly repository: DataCardRepository;
    private readonly settingsClient: ISettingsClient;
    private readonly dataClient: IDataClient;

    public constructor(settingsClient: ISettingsClient, dataClient: IDataClient) {
        this.repository = getCustomRepository(DataCardRepository);
        this.settingsClient = settingsClient;
        this.dataClient = dataClient;
    }

    public async getDataCard(id: number): Promise<DataCard> {
        return this.repository.getDataCard(id);
    }

    public async getDataCards(dashboardId: number): Promise<DataCard[]> {
        return this.repository.getDataCards(dashboardId);
    }

    public async saveDataCard(dataCardInput: ISaveDataCardInput): Promise<number> {
        const dataCard = await this.repository.saveDataCard(dataCardInput);
        return dataCard.id;
    }

    public async deleteDataCard(id: number): Promise<void> {
        await this.repository.deleteDataCard(id);
    }

    public async getResolvedData(dataCardTypeId: number, subject: number): Promise<any> {
        const dataCardType = await this.settingsClient.getDataCardType(dataCardTypeId);
        let data;
        switch (dataCardType.config.displayType) {
            case DisplayTypes.Chart:
                data = await this.resolveChartData(dataCardType, subject);
                break;
            case DisplayTypes.InfoBox:
                data = await this.resolveInfoBoxData(dataCardType, subject);
                break;
            default:
                throw new Error(`Unrecognized data card display type: ${dataCardType.config.displayType}`);
        }
        return { cardData: { ...dataCardType.config, data } };
    }

    private async resolveChartData(dataCardType: IDataCardType, subject: number): Promise<any> {
        const dataQueryParams = dataCardType.dataQuery[0].variables.reduce((res, variable) => {
            if (variable.exportFrom === ExportFromTypes.Subject) {
                return {
                    ...res,
                    [variable.exportAs]: subject,
                };
            }
            if (variable.exportValue) {
                return {
                    ...res,
                    [variable.exportAs]: variable.exportValue,
                };
            }
        }, {});

        const rawData = await this.dataClient.getData(dataCardType.dataQuery[0].id, dataQueryParams);

        return rawData.map((dataItem: any) => {
            return dataCardType.variables.reduce((res, variable) => {
                if (variable.exportFrom === ExportFromTypes.DataQuery) {
                    return {
                        ...res,
                        [variable.exportAs]: Number(dataItem[variable.exportKey]),
                    };
                }
            }, {});
        });
    }

    private async resolveInfoBoxData(dataCardType: IDataCardType, subject: number): Promise<any> {
        const data = [];
        for (let i = 0; i < dataCardType.dataQuery.length; i++) {
            const dataQueryParams = dataCardType.dataQuery[i].variables.reduce((res, variable) => {
                if (variable.exportFrom === ExportFromTypes.Subject) {
                    return {
                        ...res,
                        [variable.exportAs]: subject,
                    };
                }
                if (variable.exportValue) {
                    return {
                        ...res,
                        [variable.exportAs]: variable.exportValue,
                    };
                }
            }, {});

            const rawData = (await this.dataClient.getData(dataCardType.dataQuery[i].id, dataQueryParams))[0];

            data.push({
                title: dataCardType.variables[i].text,
                [dataCardType.variables[i].exportAs]: Number(rawData[dataCardType.variables[i].exportKey]),
            });
        }

        return data;
    }
}

export default DataCardManager;
