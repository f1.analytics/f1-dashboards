import { getCustomRepository } from 'typeorm';

import { ISettingsClient } from '../../clients/settingsClient';
import Dashboard from '../../database/entity/dashboard';
import { PersistenceErrorTypes } from '../../database/persistence';
import DashboardRepository from '../../database/repository/dashboardRepository';
import { DashboardNotFoundError, UnauthorizedError } from '../../errors/requestErrors';
import { IPutDashboardInput, IRichDashboard, ISaveDashboardInput, IUser } from '../../types';
import { IDataCardManager } from '../dataCardManager';
import { IDashboardManager } from './dashboardManager.types';

class DashboardManager implements IDashboardManager {
    private readonly repository: DashboardRepository;
    private readonly dataCardManager: IDataCardManager;
    private readonly settingsClient: ISettingsClient;

    public constructor(settingsClient: ISettingsClient, dataCardManager: IDataCardManager) {
        this.settingsClient = settingsClient;
        this.repository = getCustomRepository(DashboardRepository);
        this.dataCardManager = dataCardManager;
    }

    public async getDashboards(userId?: number): Promise<Dashboard[]> {
        return this.repository.getDashboards(userId);
    }

    public async getDashboard(id: number): Promise<IRichDashboard> {
        try {
            const dashboard = await this.repository.getDashboard(id);
            const dashboardType = await this.settingsClient.getDashboardType(dashboard.dashboardTypeId);
            return {
                ...dashboard,
                dashboardType,
            };
        } catch (error) {
            if (error.name === PersistenceErrorTypes.entityNotFound) {
                throw new DashboardNotFoundError(`Dashboard ${id} not found`);
            } else {
                throw error;
            }
        }
    }

    public async createDashboard(dashboard: ISaveDashboardInput): Promise<number> {
        const result = await this.repository.saveDashboard(dashboard);
        return result.id;
    }

    public async updateDashboard(dashboardInput: IPutDashboardInput): Promise<void> {
        await this.repository.updateDashboard(dashboardInput);
    }

    public async deleteDashboards(userId: number): Promise<void> {
        await this.repository.deleteDashboards(userId);
    }

    public async deleteDashboard(id: number): Promise<void> {
        await this.repository.deleteDashboard(id);
    }

    public async checkOwnership(dashboardId: number, user: IUser): Promise<void> {
        const dashboard = await this.getDashboard(dashboardId);

        if (dashboard && dashboard.userId && dashboard.userId !== Number(user.id) && !user.isSuperuser) {
            throw new UnauthorizedError(`User ${user.id} is not authorized to access dashboard ${dashboard.id}`);
        }
    }
}

export default DashboardManager;
