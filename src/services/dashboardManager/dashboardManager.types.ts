import Dashboard from '../../database/entity/dashboard';
import { IPutDashboardInput, IRichDashboard, ISaveDashboardInput, IUser } from '../../types';

export interface IDashboardManager {
    getDashboards(userId?: number): Promise<Dashboard[]>;
    getDashboard(id: number): Promise<IRichDashboard>;
    createDashboard(dashboard: ISaveDashboardInput): Promise<number>;
    updateDashboard(dashboardInput: IPutDashboardInput): Promise<void>;
    deleteDashboards(userId: number): Promise<void>;
    deleteDashboard(id: number): Promise<void>;
    checkOwnership(dashboardId: number, user: IUser): Promise<void>;
}
