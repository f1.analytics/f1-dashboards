import { IRestService } from './restService';

class Server {
    private readonly restService: IRestService;

    public constructor(restService: IRestService) {
        this.restService = restService;
    }

    public async start(): Promise<void> {
        this.restService.init();
        this.restService.start();
    }

    public getRestService(): IRestService {
        return this.restService;
    }
}

export default Server;
