import { ISubjectSelection } from '../../services/subjectSelectionService';
import { IDashboardType, IDataCardType } from '../../types';

export interface ISettingsClient {
    getSubjectSelection(subjectSelectionId: number): Promise<ISubjectSelection>;
    getDashboardType(dashboardTypeId: number): Promise<IDashboardType>;
    getDataCardType(id: number): Promise<IDataCardType>;
}
