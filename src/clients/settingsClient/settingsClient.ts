import axios from 'axios';

import { ISubjectSelection } from '../../services/subjectSelectionService';
import { ITokenManager } from '../../tools/tokenManager';
import { IDashboardType, IDataCardType } from '../../types';
import { ISettingsClient } from './settingClient.types';

class SettingsClient implements ISettingsClient {
    private readonly host: string;
    private readonly tokenManager: ITokenManager;

    public constructor(host: string, tokenManager: ITokenManager) {
        this.host = host;
        this.tokenManager = tokenManager;
    }

    public async getSubjectSelection(subjectSelectionId: number): Promise<ISubjectSelection> {
        try {
            const { data } = await axios.get(`${this.host}/subjectSelections/${subjectSelectionId}`, {
                headers: {
                    authorization: this.tokenManager.getStaticToken(),
                },
            });
            return data;
        } catch (error) {
            if (error?.response.data.error) {
                throw new Error(error.response.data.error.message);
            } else {
                throw new Error(error.message);
            }
        }
    }

    public async getDashboardType(dashboardTypeId: number): Promise<IDashboardType> {
        try {
            const { data } = await axios.get(`${this.host}/dashboardTypes/${dashboardTypeId}`, {
                headers: {
                    authorization: this.tokenManager.getStaticToken(),
                },
            });
            return data;
        } catch (error) {
            if (error?.response.data.error) {
                throw new Error(error.response.data.error.message);
            } else {
                throw new Error(error.message);
            }
        }
    }

    public async getDataCardType(id: number): Promise<IDataCardType> {
        try {
            const { data } = await axios.get(`${this.host}/dataCardTypes/${id}`, {
                headers: {
                    authorization: this.tokenManager.getStaticToken(),
                },
            });
            return data;
        } catch (error) {
            if (error?.response.data.error) {
                throw new Error(error.response.data.error.message);
            } else {
                throw new Error(error.message);
            }
        }
    }
}

export default SettingsClient;
