export interface IDataClient {
    getData(dataQueryOptionId: number, query?: object): Promise<any>;
}

export interface IDataParams {
    name: string;
    [index: string]: any;
}
