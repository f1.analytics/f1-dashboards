import axios from 'axios';

import { BadRequestError } from '../../errors/requestErrors';
import { ITokenManager } from '../../tools/tokenManager';
import { IDataClient, IDataParams } from './dataClient.types';

const BAD_DATA_QUERY = 'BadDataQuery';

class DataClient implements IDataClient {
    private readonly host: string;
    private readonly tokenManager: ITokenManager;

    public constructor(host: string, tokenManager: ITokenManager) {
        this.host = host;
        this.tokenManager = tokenManager;
    }

    public async getData(dataQueryOptionId: number, params?: IDataParams): Promise<any> {
        try {
            const stringParams = params ? this.stringifyRequestParams(params) : '';
            const { data } = await axios.get(`${this.host}/${dataQueryOptionId}?${stringParams}`, {
                headers: {
                    authorization: this.tokenManager.getStaticToken(),
                },
            });
            return data;
        } catch (error) {
            if (error?.response.data.error) {
                if (error.response.data.error.code === BAD_DATA_QUERY) {
                    throw new BadRequestError(`Data client error: ${error.response.data.error.message}`);
                }
                throw new Error(error.response.data.error.message);
            } else {
                throw new Error(error.message);
            }
        }
    }

    private stringifyRequestParams(params: IDataParams): string {
        return Object.keys(params)
            .map((key) => `${key}=${params[key]}`)
            .join('&');
    }
}

export default DataClient;
