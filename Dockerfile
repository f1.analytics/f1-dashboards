FROM node:8
WORKDIR /usr/src/f1-dashboards
COPY . .
RUN npm install
EXPOSE 80
CMD [ "npm", "start" ]
